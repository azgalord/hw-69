import React, {Component} from 'react';
import {connect} from 'react-redux';

import MenuItem from "../../component/MenuItem/MenuItem";
import './Menu.css';

import {fetchProducts} from "../../store/actions/actionTypes";
import {addProduct} from "../../store/actions/cart";

class Menu extends Component {
  componentDidMount() {
    this.props.fetchProducts();
  }

  render() {
    return (
      <div className="Menu">
        {this.props.products.map((product) => (
          <MenuItem
            image={product.image}
            name={product.name}
            price={product.price}
            add={() => this.props.addProduct(product.id)}
            key={product.id}
          />
        ))}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    products: state.menu.products,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    fetchProducts: () => dispatch(fetchProducts()),
    addProduct: id => dispatch(addProduct(id)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Menu);
