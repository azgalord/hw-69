import React, {Component} from 'react';
import {connect} from 'react-redux';

import CartItem from "../../component/CartItem/CartItem"
import {deleteProduct} from "../../store/actions/cart";
import axios from '../../axios-menu';

import './Cart.css';
import Modal from "../../component/UI/Modal/Modal";
import Form from "../../component/Form/Form";

class Cart extends Component {
  state = {
    modal: false,
    name: '',
    address: '',
    number: ''
  };

  openModal = () => {
    this.setState({modal: !this.state.modal});
  };

  changeHandler = (event) => {
    this.setState({[event.target.name] : event.target.value});
  };

  placeOrder = (event) => {
    event.preventDefault();
    const orders = {};

    Object.values(this.props.cartItems).map((item, id) => {
      if (item.amount) {
        orders[id] = {
          name: this.props.items[id],
          amount: item.amount
        };
      }
      return item; // просто что то надо было вернуть))
    });

    const readyOrder = {
      orders: orders,
      name: this.state.name,
      address: this.state.address,
      number: this.state.number
    };

    axios.post('/cafeOrders.json', readyOrder).then(() => {
      this.openModal();
    });
  };

  render() {
    return (
      <div className="Cart">
        <Modal close={() => this.openModal()} visible={this.state.modal}>
          <Form
            name={this.state.name} address={this.state.address}
            number={this.state.number} change={(event) => this.changeHandler(event)}
            send={(event) => this.placeOrder(event)}
          />
        </Modal>

        <div className="CartItems">
          {Object.values(this.props.cartItems).map((cartItem, index) => {
            const values = Object.keys(this.props.cartItems);
            if (cartItem.amount) {
              return <CartItem name={this.props.items[index]} amount={cartItem.amount} price={cartItem.price} key={index} del={() => this.props.deleteProduct(values[index])}/>
            }
            return null;
          })}
        </div>
        <div className="CartPrice">
          <span>Delivery: {this.props.delivery}</span>
          <span>Total: {this.props.total}</span>
        </div>
        <button onClick={() => this.openModal()} disabled={(this.props.total > 150) ? false : true} className="CartButton">Place order</button>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    cartItems: state.cart.cartItems,
    items: state.cart.items,
    total: state.cart.total,
    delivery: state.cart.delivery
  }
};

const mapDispatchToProps = dispatch => {
  return {
    deleteProduct: product => dispatch(deleteProduct(product))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
