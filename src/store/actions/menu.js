import {FETCH_MENU_REQUEST, MENU_SUCCESS} from "./actionTypes";

export const fetchMenuRequest = () => ({type: FETCH_MENU_REQUEST});
export const fetchMenuSuccess = (products) => ({type: MENU_SUCCESS, products});

