import {fetchMenuRequest, fetchMenuSuccess} from "./menu";
import axios from '../../axios-menu';

export const ADD_PRODUCT = 'ADD_PRODUCT';
export const DELETE_PRODUCT = 'DELETE_PRODUCT';

export const MENU_SUCCESS = 'MENU_SUCCESS';
export const FETCH_MENU_REQUEST = 'FETCH_MENU_REQUEST';

export const fetchProducts = () => {
  return dispatch => {
    dispatch(fetchMenuRequest());
    axios.get('/products.json').then(response => {
      dispatch(fetchMenuSuccess(response.data));
    })
  }
};
