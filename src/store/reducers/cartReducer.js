import {ADD_PRODUCT, DELETE_PRODUCT} from "../actions/actionTypes";

const INITIAL_PRODUCTS = {
  cocaCola: 120,
  olivie: 240,
  greekSalad: 180,
  cubanCigars: 1800,
  foieGras: 650,
};

const DELIVERY = 150;

const INITIAL_STATE = {
  cartItems: {
    cocaCola: {amount: 0, price: 0},
    olivie: {amount: 0, price: 0},
    greekSalad: {amount: 0, price: 0},
    cubanCigars: {amount: 0, price: 0},
    foieGras: {amount: 0, price: 0},
  },
  items: ['Coca-Cola', 'Olivie', 'Greek Salad', 'Cuban cigars', 'Foie Gras'],
  total: DELIVERY,
  delivery: DELIVERY
};

const initialState = {...INITIAL_STATE};

const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case (ADD_PRODUCT):
      const cartItems = {...state.cartItems};
      cartItems[action.product].amount += 1;
      cartItems[action.product].price = state.cartItems[action.product].amount * INITIAL_PRODUCTS[action.product];

      return {
        ...state,
        cartItems: cartItems,
        total: state.total + INITIAL_PRODUCTS[action.product],
      };
    case (DELETE_PRODUCT):
      const itemsCart = {...state.cartItems};
      const thisPrice = itemsCart[action.product].price;
      itemsCart[action.product].amount = 0;
      itemsCart[action.product].price = 0;

      return {
        ...state,
        cartItems: itemsCart,
        total: state.total - thisPrice,
      };
    default:
      return state;
  }
};

export default cartReducer;
