import {MENU_SUCCESS} from "../actions/actionTypes";

const initialState = {
  products: [],
};

const menuReducer = (state = initialState, action) => {
  switch (action.type) {
    case (MENU_SUCCESS):
      return {
        ...state,
        products: action.products
      };
    default:
      return state;
  }
};

export default menuReducer;
