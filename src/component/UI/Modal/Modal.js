import React from 'react';
import './Modal.css';

const Modal = ({children, visible, close}) => {
  return (
    <div style={{display: visible ? 'block' : 'none'}} className="ModalBg">
      <div className="Modal">
        {children}
        <button onClick={close} className="ModalClose">X</button>
      </div>
    </div>
  );
};

export default Modal;
