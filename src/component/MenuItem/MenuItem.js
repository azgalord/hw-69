import React from 'react';

import './MenuItem.css';

const MenuItem = ({image, name, price, add}) => {
  return (
    <div className="MenuItem">
      <img src={image} alt=""/>
      <div className="MenuItemText">
        <h2>{name}</h2>
        <span>KGS {price}</span>
      </div>
      <button onClick={add}>Add to cart</button>
    </div>
  );
};

export default MenuItem;
