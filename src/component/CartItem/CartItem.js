import React from 'react';

import './CartItem.css';

const CartItem = ({name, amount, price, del}) => {
  return (
    <div className="CartItem">
      <p>{name} x{amount}</p>
      <span>{price} KGS</span>
      <button onClick={del}>X</button>
    </div>
  );
};

export default CartItem;
