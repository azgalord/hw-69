import React from 'react';

import './Form.css';

const Form = ({name, address, number, change, send}) => {
  return (
    <form>
      <input type="text" name="name" value={name} onChange={change} placeholder="Your name"/>
      <input type="text" name="address" value={address} onChange={change} placeholder="Your address"/>
      <input type="number" name="number" value={number} onChange={change} placeholder="Your phone number"/>
      <button type="submit" onClick={send}>Send Order</button>
    </form>
  );
};

export default Form;
