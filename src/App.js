import React, { Component } from 'react';
import Menu from "./containers/Menu/Menu";
import Cart from "./containers/Cart/Cart";

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Menu/>
        <Cart/>
      </div>
    );
  }
}

export default App;
